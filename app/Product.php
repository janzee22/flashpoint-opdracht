<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['productnaam','SKU','EAN','verkoopprijs','segment','inhoud','voedingswaarde','actuele_voorraad','merk_id'];
}
