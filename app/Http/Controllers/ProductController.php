<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ProductCollection;
use App\Product;

class ProductController extends Controller
{
    public function store(Request $request){
        $product = new Product([
            'productnaam' => $request->get('productnaam'),
            'SKU' => $request->get('SKU'),
            'EAN' => $request->get('EAN'),
            'verkoopprijs' => $request->get('verkoopprijs'),
            'segment' => $request->get('segment'),
            'inhoud' => $request->get('inhoud'),
            'voedingswaarde' => $request->get('voedingswaarde'),
            'actuele_voorraad' => $request->get('actuele_voorraad'),
            'merk_id' => $request->get('merk_id')
        ]);
        $product->save();

        return response()->json('success');
    }
    public function index(){
        return new ProductCollection(Product::all());
    }
}
