<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ProductCollection;
use App\Product;
use Storage;
use App\Brand;

class DatabaseFillController extends Controller
{
    public function fill(){

        $json = Storage::disk('local')->get('data.json');
        $json = json_decode($json, true);
        foreach($json as $key => $value){
            if($key == "merken"){
                Brand::insert($value);
            }
            if($key == "products"){
                Product::insert($value);
                return response()->json('success');
            }
        }

        return response()->json('failed');
    }
}
